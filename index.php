<?php

ini_set('memory_limit', '-1');

require_once __DIR__.'/vendor/autoload.php';

use Symfony\Component\Config\FileLocator;
use Symfony\Component\Routing\Loader\YamlFileLoader as YamlRouting;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Nicl\Silex\MarkdownServiceProvider;


use Services\dataProcess;
use Services\dataLoader;
use Services\dataLoaderServiceProvider;
use Services\twigYearsToUrl;


$app = new Silex\Application();

$app->register(new DerAlex\Silex\YamlConfigServiceProvider(__DIR__ . '/settings.yml'));

$app['debug'] = $app['config']['debug'];


// Procesamos los datos

  // lector de datos JSON
$app['dataLoader'] = function () { return new dataLoader(); };

  /* Importando listado completo canciones */
$songsFile = './data/songsES.json';
$app['dataLoader']->getData($app, $songsFile, 'songs', 'json'); // -> nos genera $app['dataLoader.songs']

  /* Importando listado completo canciones */
$songsFile = './data/regions.json';
$app['dataLoader']->getData($app, $songsFile, 'regions', 'json'); // -> nos genera $app['dataLoader.regions']

  /* Importando listado completo canciones */
$songsFile = './data/spainvotes.json';
$app['dataLoader']->getData($app, $songsFile, 'spainvotes', 'json'); // -> nos genera $app['dataLoader.spainvotes']

  /* Importando listado completo canciones */
$songsFile = './data/spanishPositions.json';
$app['dataLoader']->getData($app, $songsFile, 'spanishPositions', 'json'); // -> nos genera $app['dataLoader.spanishPositions']

  /* Importando listado completo canciones */
$songsFile = './data/partis.json';
$app['dataLoader']->getData($app, $songsFile, 'partis', 'json'); // -> nos genera $app['dataLoader.partis']

  /* Importando seo */
$seoFile = './data/victors.json';
$app['dataLoader']->getData($app, $seoFile, 'victors', 'json'); // -> nos genera $app['dataLoader.victors']

  /* Importando seo */
$seoFile = './data/countries.geo.json';
$app['dataLoader']->getData($app, $seoFile, 'geo', 'json'); // -> nos genera $app['dataLoader.geo']

  /* Importando seo */

$seoFile = './data/seo.csv';
$app['dataLoader']->getData($app, $seoFile, 'seo', 'csv', 'url'); // -> nos genera $app['dataLoader.stats']


//TWIG
$app->register(new Silex\Provider\TwigServiceProvider(), array( 'twig.path' => __DIR__.'/twigs', ));

$app['twig'] = $app->share($app->extend('twig', function ($twig, $app) {
    /* sample Twig filter
    $twig->addExtension(new Services\twigYearsToUrl($app));*/
    return $twig; 
}));

$app['routes'] = $app->extend('routes', function (RouteCollection $routes, $app) {
    $loader     = new YamlRouting(new FileLocator(__DIR__ . '/'));
    $collection = $loader->load('routes.yml');
    $routes->addCollection($collection);

    return $routes;
});

$app->error(function (\Exception $e, $code) use($app) {
  if(!$app['debug']) {
    return new Response($app['twig']->render('error.html.twig'), $code);
  }
});

$app->register(new Silex\Provider\UrlGeneratorServiceProvider());

$app->run();