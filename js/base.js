
/*    ________________________________________________
     _____/\/\____________/\/\______/\/\/\/\/\_______
    _____/\/\__________/\/\/\/\____/\/\____/\/\_____
   _____/\/\________/\/\____/\/\__/\/\/\/\/\_______
  _____/\/\________/\/\/\/\/\/\__/\/\____/\/\_____
 _____/\/\/\/\/\__/\/\____/\/\__/\/\/\/\/\_______
________________________________________________
          ________________________________________________________________ 
         _____/\/\/\/\/\____/\/\/\/\/\/\__/\/\____/\/\__/\/\/\/\/\/\_____ 
        _____/\/\____/\/\______/\/\______/\/\____/\/\__/\_______________ 
       _____/\/\/\/\/\________/\/\______/\/\____/\/\__/\/\/\/\/\_______ 
      _____/\/\__/\/\________/\/\________/\/\/\/\____/\/\_____________ 
     _____/\/\____/\/\______/\/\__________/\/\______/\/\/\/\/\/\_____ 
    ________________________________________________________________ 
____________ Diseño: Ismael Recio, Redacción: Alberto Fernández / Miriam Hernanz, Realización: César Vallejo / Miguel Campos 
Desarrollo: David Ruiz / Francisco Quintero / Carlos Jiménez Delgado @2013__________________________ */

'use strict';
/*global labTools */
/*global Modernizr */
/*global $ */

// Objeto principal 
// -----------------------------------------------------------------------------------

var evData = {
  init: function() {
    // gestión de urls____________________________________________________________________
    if(labTools.url) {
      labTools.url.initiate(evData.handleUrlOnReady, evData.handleUrlOnChange);
    }

      // inicializando video____________________________________________________________
    labTools.media.init();

    evData.secciones.init();

    evData.initializacion();

    if(Modernizr.touch) $('.togglesound').click();

  },
          // función control de las urls______________________________________________________
  handleUrlOnChange: function(url) {

  },
  handleUrlOnReady: function(url) {

  },

  initializacion: function() {
        // links que abren videos
    $('body').delegate('.govideo, .govideo2', 'click', function(){ evData.dataTools.videoPopup($(this).data('year'),$(this).data('country')); return false; });

        // links que abren videos
    $('body').delegate('.toggleclass', 'click', function(){ $(this).parents('.bl-sct').toggleClass($(this).data('toggleclass')); return false; });

      // listado completo de canciones
    $('body').delegate('.temazo', 'click', function(){ evData.dataTools.videoPopup($(this).data('year'),$(this).data('country')); return false; });

      // esconde popup de video
    $('.closevideo').click(function(){ 
      evData.dataTools.clearVideoPopup();
      return false; 
    })

    $('.goto-link').click(function(){
      evData.secciones.goTo($(this).data('goto'));

      return false;
    });

    evData.menu.init($('#container-menu'));
    evData.menu.tooltip();

  },

  url: {

    data: {},

    goTo: function (id) {

      labTools.console.log("Ajustando menu a: " + id);

      var cadenaBuscada = id;
      var url = "";

      if ( cadenaBuscada.indexOf("#") != 0 ) {
        cadenaBuscada = "#" + cadenaBuscada;
      }

      var encontrado = false;
      var indice = 0;

      var $elemento = null; 

      var menu = $("ul.bl-main-menu li");

      for (indice = 0; indice < menu.size() && !encontrado; indice++ ){

        var elemento = menu[indice];
        var $elemento = $(elemento).children('a'); 

        if ( $elemento.attr("data-goto") === cadenaBuscada ) {
          encontrado = true;

          url = $elemento.attr("href");

        }

      }

      if ( encontrado ) {

        // Establecemos la url
        labTools.url.setUrl(url);
      } 

    }

  }, 

  menu : {
    data: {
      timer: null,
      count: 0
    },
  
    init: function ($menu) {

      //console.log("Puto menu");
     
      $('.rs-carousel-action-prev').click(evData.menu.prev);
      $('.rs-carousel-action-next').click(evData.menu.next);


      if ( Modernizr.touch ) {

        // Inicializacion de eventos para dispositivos tactiles

        $('.rs-carousel-action-prev').click(function() {
          evData.menu.prev();
        });


        $('.rs-carousel-action-next').click(function() {
          evData.menu.next();
        }); 

      } else {

        // Inicializacion de eventos para PC

        $('.rs-carousel-action-prev').mouseenter(function() {
            evData.menu.prev();
            evData.menu.data.timer = setInterval(function(){
              evData.menu.prev();
            },300);
        });


        $('.rs-carousel-action-next').mouseenter(function() {
            evData.menu.next();
            evData.menu.data.timer = setInterval(function(){
              evData.menu.next();
            },300);
        });    


        $('.rs-carousel-action-prev').mouseout(function() {
          clearInterval(evData.menu.data.timer);
        });

        $('.rs-carousel-action-next').mouseout(function() {
          clearInterval(evData.menu.data.timer);
        });

      }



    },

    goTo: function (id) {

      labTools.console.log("Ajustando menu a: " + id);

      var cadenaBuscada = id;

      if ( cadenaBuscada.indexOf("#") != 0 ) {
        cadenaBuscada = "#" + cadenaBuscada;
      }

      var encontrado = false;
      var indice = 0;

      var $elemento = null; 

      var menu = $("ul.bl-main-menu li");

      for (indice = 0; indice < menu.size() && !encontrado; indice++ ){

        var elemento = menu[indice];
        var $elemento = $(elemento).children('a'); 

        if ( $elemento.attr("data-goto") === cadenaBuscada ) {
          encontrado = true;
        }

      }

      if ( encontrado ) {

        evData.menu.data.count = indice-1;

        labTools.console.log("Indice: " + evData.menu.data.count);

        var $menu = $('#container-menu .bl-main-menu');

        $menu.stop(false, false).animate({ scrollTop: 124*(indice-1) },500);

      } 

    },

    next: function () {
      labTools.console.log("Pulso next");

      var $menu = $('#container-menu .bl-main-menu');
      var $menuInn = $('.rs-carousel-item-container');
      var scrollTop = $menu.scrollTop();
      var heightMenu = $menuInn.height();
      var heightMenuReal = 500;

      if (scrollTop + heightMenuReal > heightMenu) return false;

      $menu.stop(false, false).animate({ scrollTop: scrollTop+200 },500);

      return false;
    },

    prev: function () {
      labTools.console.log("Pulso prev");

      var $menu = $('#container-menu .bl-main-menu');
      var $menuInn = $('.rs-carousel-item-container');
      var scrollTop = $menu.scrollTop();
      var heightMenu = $menuInn.height();
      var heightMenuReal = 500;

      if (scrollTop < 0) return false;

      $menu.stop(false, false).animate({ scrollTop: scrollTop-200 },500);

      return false;
    },

    tooltip: function(){
        $('.t-tip').mouseover(function(){
          var $this = $(this);
          var texto = $this.data('ttip');
          var prevtip = $this.find('.t-tip-wrap');
          $this.append('<div class="t-tip-wrap"><div class="t-tip-elm">' + texto + '</div></div>');
          $this.find('.t-tip-wrap').show();
        }).mouseout(function(e){ 
          $(this).find('.t-tip-wrap').hide();
        });
    } 
  },


  secciones: {

    data: {
      actClass: 'sct-active',

      active: '#sct-portada',

      previus: ''
    },

    init: function () {


    },
    goTo: function(IDsection, params) {

      // mostramos la seccion especificada con el IDsection

      if ( typeof IDsection != "undefined" ) {

        // Asignamos la seccion a la que queremos ir
        var $seccion = $(IDsection);

        // Comprobamos que exista
        if ( typeof $seccion != "undefined" ) {

          evData.secciones.data.previus = evData.secciones.data.active;
          evData.secciones.data.active = IDsection;

          evData.dataTools.clearVideoPopup();

          $(evData.secciones.data.previus).removeClass(evData.secciones.data.actClass);//.hide();
          $(evData.secciones.data.active).addClass(evData.secciones.data.actClass);//show();

          $('.bl-main-menu').find('.act').removeClass('act');
          $('.bl-main-menu').find('[data-goto="' + evData.secciones.data.active + '"]').addClass('act');

          var callbackIntro = null, callbackOut = null;
          var funcIn = $(evData.secciones.data.active).data("callbackin");
          if(funcIn) callbackIntro = evData.secciones.callCallback(funcIn, window, params);

          var funcOut = $(evData.secciones.data.previus).data("callbackout");
          if(funcOut) callbackOut = evData.secciones.callCallback(funcOut, window, params);

          labTools.console.log('[go] Yendo a sección ' + IDsection);
          
          // Establecemos el menu en la posicion correspondiente
          evData.menu.goTo(IDsection); 

          evData.url.goTo(IDsection);

          // ejecutamos la funcion
          if (typeof callbackIntro != "undefined" && typeof callbackIntro === "function") {

            labTools.console.log('[go] Ejecutando callback de la sección ' + IDsection);

            callbackIntro();
          }

        } else {
          labTools.console.log("[go] La seccion especificada con el ID: " + IDsection + " no existe.");
        }


      } else {
        labTools.console.log("[go] ID de seccion no especificada");
      }

      evData.cache.initialLoad = false;

    },
      // quitamos el eval http://stackoverflow.com/questions/359788/how-to-execute-a-javascript-function-when-i-have-its-name-as-a-string
    callCallback: function(functionName, contex, params){console.log(functionName)
      var ctx = contex;
      var args = [].slice.call(arguments).splice(2);
      var namespaces = functionName.split(".");
      var func = namespaces.pop();
      for(var i = 0; i < namespaces.length; i++) {
        ctx = ctx[namespaces[i]];
      }
      return ctx[func].apply(this, args);
    },
  },

  dataTools: {
    getSong: function (year, country) { // devuelve datos de canción y año -> vData.dataTools.getSong(1972, 'España');

      var songs = evData.data.songs, songsLength = songs.length;

      for (var i = 0; i < songsLength; i++) {
        if(songs[i].country == country && songs[i].year == year) { return songs[i]; };
      };

      return false;  
    },

    getWins: function (country) { // devuelve datos de canción y año -> vData.dataTools.getSong(1972, 'España');

      var wins = evData.data.victors, songsLength = wins.length;
      for(var count in evData.data.victors) {
        if( count == country ) return evData.data.victors[count];
      }

      return false;  
    },

    getCountry: function (country) {

      for (var i = 0; i < evData.data.partis.length; i++) {
        if(evData.data.partis[i].nameEN == country) return evData.data.partis[i];
      };
      labTools.console.log('pais not fount: '+country);
      return false;
    },

    clearVideoPopup: function() {
      $('#tpl-video').removeClass('sct-active').find('.bl-vid-vid').html('');
    },

    videoPopup: function(year, country) {

      if(!year || !country) { labTools.console.log('[pV] error solicitando video'); return false;}

      var song = evData.dataTools.getSong(year, country);

      if(!song) { labTools.console.log('[pV] video no encontrado'); return false; };

      var $tpl = $('#tpl-video');

      var vid = song.youtube.split('v=')[1];

      $tpl.find('.bl-vid-vid').html('<iframe width="824" height="462" src="//www.youtube.com/embed/'+vid+'" frameborder="0" allowfullscreen></iframe>')
      $tpl.find('h2').html(song.country + ' ' + song.year + ' - Posición: '+ song.posicion);
      $tpl.find('.bl-vid-song').html('Canción: ' + song.titulo);
      $tpl.find('.bl-vid-artis').html('Intérprete: ' + song.artista)

      $tpl.addClass('sct-active');
    },

    processAngularSongs: function() {

      var countries = [];
      var years = [];
      var positions = [];

      var data = evData.data.songs;
      var lngth = data.length;

      for (var i = lngth - 1; i >= 0; i--) {
        if(countries.indexOf(data[i]['country']) == -1) countries.push(data[i]['country']);
        if(years.indexOf(data[i]['year']) == -1) years.push(data[i]['year']);
        if(positions.indexOf(data[i]['posicion']) == -1) positions.push(parseInt(data[i]['posicion']));
        data[i]['posicion'] = parseInt(data[i]['posicion']);
      };

      return {countries: countries, years: years, positions: positions};
    },
    getCountryParticipations: function ( country ){
      var len = evData.data.victors.length;

      for(var victor in evData.data.victors){ 
        if(victor == country) return evData.data.victors[victor];
      }

      return false;

    },
    translateCountry2ES: function(country){ // evData.dataTools.translateCountry2ES

      for (var i = 0; i < evData.data.partis.length; i++) {
        if(evData.data.partis[i].nameEN == country) return evData.data.partis[i].name;
      };

      return false;
    },
    getCentroid: function(element) { // vData.dataTools.getCentroid //http://stackoverflow.com/questions/12062561/calculate-svg-path-centroid-with-d3-js
      // get the DOM element from a D3 selection
      // you could also use "this" inside .each()
          // use the native SVG interface to get the bounding box
      var bbox = element.getBBox();
      // return the center of the bounding box
      return [bbox.x + bbox.width/2, bbox.y + bbox.height/2];
    }
  }


};


/* Microlibería para generación de tramos de arcos https://github.com/drlz/vGCal */
  //simplifica angulos (elimina vueltas extras) 
var aSimpli = function(ang){
  if (ang > 2*Math.PI){ ang=ang%(Math.floor(ang/(2*Math.PI))*2*Math.PI);return ang;};
  if (ang < -(2*Math.PI)){ ang=ang%(Math.ceil(ang/(2*Math.PI))*2*Math.PI);return ang;};
  return ang;
}

  /*generador de arcos -> devuelve trazado! 
    (angulo de inicio del arco (rad),
      angulo final del arco,
      centro_x en pixeles,
      centro_y en pixeles,
      radio interior en pixeles,
      grosor del arco en pixeles
    ) */
var rphArc  = function (a_start,a_end,centro_x,centro_y,radio,grosor){
    
  //si uno de los dos arcos es > que un círculo quitamos las vueltas extras
  var a_start = aSimpli(a_start),
  a_end= aSimpli(a_end);

  //generamos desde el angulo menor al mayor
  if(a_start>a_end) { var tmp = a_end; a_end= a_start; a_start= tmp;}

  //valor de svg large-arc-flag
  var large;
  a_end-a_start>Math.PI ? large = 1 : large = 0;

  var radio2= radio+grosor,
    path='M'+(centro_x+Math.cos(a_start)*radio)+','+(centro_y+Math.sin(a_start)*radio)+
      ' A'+radio+','+radio+' 0 '+ large +',1 '+(centro_x+Math.cos(a_end)*radio)+','+(centro_y+Math.sin(a_end)*radio)+
      ' L'+(centro_x+Math.cos(a_end)*radio2)+','+(centro_y+Math.sin(a_end)*radio2)+
      ' A'+radio2+','+radio2+' 0 ' + large + ',0 ' +(centro_x+Math.cos(a_start)*radio2)+','+(centro_y+Math.sin(a_start)*radio2)+'Z';
  return path;
};

// Almacenamiento en general
// -----------------------------------------------------------------------------------
evData.cache = {

  disableAudio: false,
  initialLoad: true

};


// DOCUMENT READY
// ----------------------------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------------

$( document ).ready(function() {

  evData.init();


});

// http://www.paulirish.com/2011/requestanimationframe-for-smart-animating/
// shim layer with setTimeout fallback
window.requestAnimFrame = (function(){
  return  window.requestAnimationFrame       ||
          window.webkitRequestAnimationFrame ||
          window.mozRequestAnimationFrame    ||
          function( callback ){
            window.setTimeout(callback, 1000 / 60);
          };
})();