<?php

namespace Services;

/*

Servicio que carga los datos desde un fichero CSV o JSON

dataLoader.php
*/

use Silex\Application;
use Silex\ServiceProviderInterface;
use Services\FileToArrayServiceProvider as FileToArrayServiceProvider;


class dataLoaderServiceProvider implements ServiceProviderInterface
{
    public function register(Application $app)
    {

        $app['dataLoader'] = $app->protect(function ($name) use ($app) {

            $default = $app['dataLoader.file'] ? $app['dataLoader.file'] : '';
            $name = $name ?: $default;

            return $app['dataLoader'];
        });    
    }

    public function boot(Application $app)
    {
    }

    public static function getData($app)
    {
      
      if($app['dataLoader.format'] == 'csv') {
        return $app['dataLoader.data'] = FileToArrayServiceProvider::fileCSVToArray($app['dataLoader.file'], $app['dataLoader.column']);
      } elseif ( $app['dataLoader.format'] == 'json' ) {
        return $app['dataLoader.data'] = json_decode(file_get_contents($app['dataLoader.file']));
      }

    }    
}

?>