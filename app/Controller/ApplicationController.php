<?php 
namespace Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Exceptions\ValidationException;

class ApplicationController
{

    public function indexAction(Request $request, Application $app)
    {	

        $section = $this->getSectionName($request);
        $url = $this->getUrlSection($request);

        return $app['twig']->render('index.html.twig', array(
            'songs' => $app['dataLoader.songs'],
            'url' => $url,
            'section' => $section,            
            'seo' => $app['dataLoader.seo'],
            'spanishPositions' => $app['dataLoader.spanishPositions'],
            'partis' => $app['dataLoader.partis'],
            'victors' => $app['dataLoader.victors'],
            'geo' => $app['dataLoader.geo'],
            'regions' => $app['dataLoader.regions'],
            'spainvotes' => $app['dataLoader.spainvotes']
        ));

    }
/*
    public function fichaAction(Request $request, Application $app, $personaje)
    {   

        $section = $this->getSectionName($request);
        $url = $this->getUrlSection($request);

        // Comprobamos que el personaje esta entre los chefs
        if (in_array($personaje, $app['dataLoader.ºfs'][$personaje])) {
            
            return $app['twig']->render('index.html.twig', array(
            'songs' => $app['dataLoader.songs'],
            'url' => $url,
            'seo' => $app['dataLoader.seo']
            ));


        } else {

           return $app->redirect($app['url_generator']->generate('index')); // -> url no encontrada -> redirigimos a la home
        }
    }
*/
    /**
    * Get current controller name
    */
    public function getControllerName($request_attributes) {

        $pattern = "/Controller\\\\([a-zA-Z]*)Controller/";
        $matches = array();

        preg_match($pattern, $request_attributes->get("_controller"), $matches);
         
        return $matches[1];
    }

    public function getActionName($request_attributes)
    {
        if ( null !== $request_attributes ) {
            $pattern = "#::([a-zA-Z]*)Action#";
            $matches = array();
            preg_match($pattern, $request_attributes->get('_controller'), $matches);

            return $matches[1];
        }
    }   

    public function getSectionName($request_attributes)
    {
        if ( null !== $request_attributes ) {
            $pattern = "#Section";
            $replace = "";

            return str_replace($pattern,$replace,$request_attributes->get('_section'));
        }
    }

    public function getUrlSection($request_attributes)
    {
        if ( null !== $request_attributes ) {
            return $request_attributes->get('_url');
        }
    }    
}